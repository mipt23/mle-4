from connect_spark import ConnectSpark
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.classification import RandomForestClassifier, LogisticRegression
from pyspark.ml import Pipeline

class RF:
    def __init__(self, spark_obj, url, train_table = 'train', test_table = 'test', numTrees = 10):
        self.spark_obj = spark_obj
        self.train_db = spark_obj.read_db(url, train_table)
        self.test_db = spark_obj.read_db(url, test_table)
        self.numTrees = numTrees
        self.vector_assembler = VectorAssembler(inputCols=['calcium_100g', 'fat_100g', 'proteins_100g', 'energy_100g'],
                                                outputCol="features")
    
    def train(self):
        rf = RandomForestClassifier(labelCol='cluster', featuresCol='features', numTrees=self.numTrees)
        pipeline = Pipeline(stages=[self.vector_assembler, rf])
        self.model = pipeline.fit(self.train_db)
        return self.model


class LogReg:
    def __init__(self, spark_obj, url, train_table = 'train', test_table = 'test', 
                maxIter = 1000, family='multinomial'):
        
        self.spark_obj = spark_obj
        self.train_db = spark_obj.read_db(url, train_table)
        self.test_db = spark_obj.read_db(url, test_table)
        self.maxIter = maxIter
        self.family = family
        self.vector_assembler = VectorAssembler(inputCols=['calcium_100g', 'fat_100g', 'proteins_100g', 'energy_100g'],
                                                outputCol="features")
    
    def train(self):
        log_reg = LogisticRegression(labelCol='cluster', featuresCol='features', maxIter=self.maxIter, family=self.family)
        pipeline = Pipeline(stages=[self.vector_assembler, log_reg])
        self.model = pipeline.fit(self.train_db)
        return self.model
