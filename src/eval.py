from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.mllib.evaluation import MulticlassMetrics
from pyspark.sql.types import FloatType
import pyspark.sql.functions as F
from pyspark.ml.feature import Normalizer


class EvalModel:

    def __init__(self, model_obj):
        self.model_obj = model_obj
    
    def get_test_pred(self):
        self.test_pred = self.model_obj.model.transform(self.model_obj.test_db)

    def get_accuracy(self):
        accuracy_obj = MulticlassClassificationEvaluator(labelCol='cluster', predictionCol='prediction', metricName='accuracy')
        self.accuracy_val = accuracy_obj.evaluate(self.test_pred)
        print(f'Accuracy: {self.accuracy_val}')

    def get_f1(self):
        f1_obj = MulticlassClassificationEvaluator(labelCol='cluster', predictionCol='prediction', metricName='f1')
        self.f1_val = f1_obj.evaluate(self.test_pred)
        print(f'F1: {self.f1_val}')
    
    def get_cm(self):
        cm_obj = MulticlassMetrics(self.test_pred.select(['prediction', 'cluster']).withColumn('cluster',
                                                                                            F.col('cluster').cast(
                                                                                            FloatType())).orderBy(
                                                                                            'prediction').select(
                                                                                            ['prediction', 'cluster']).rdd.map(tuple))
        self.cm_val = cm_obj.confusionMatrix().toArray()
        print('Confusion matrix:\n {self.cm_val}')

    def get_confidence(self):
        df_norm = self.test_pred.rdd.map(lambda x: (x['class'], x['probability'])) \
    .reduceByKey(add).toDF(['class', 'probability'])

        norm_func = Normalizer(inputCol='probability', outputCol='normed', p=1)
        norm_func = norm_func.transform(df_norm).rdd.map(lambda x: (x['class'], x['norm_func'].toArray().max()))

        info = []
        for label, confidence in norm_func.collect():
            info.append([int(label), confidence]) 

        info = sorted(info, key=lambda x: x[0])
        for l, c in info:
            print(f'Class: {l} confidence: {c}')
