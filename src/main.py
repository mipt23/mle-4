import argparse
from spark_data import SparkData
from train import RF, LogReg
from eval import EvalModel

if __name__ == '__main__':
    #получение аргументов
    parser = argparse.ArgumentParser()
    parser.add_argument('--url', type=str, required=True)
    parser.add_argument('--in_table', type=str, default='openfoodfacts') 
    args = parser.parse_args()

    #загрузка данных
    sdata = SparkData()
    sdata.read_db(args.url, args.in_table)
    sdata.split()
    
    #обучение RF
    rf_obj = RF(sdata, args.url)
    rf_obj.train()

    #эвалюация RF
    rf_eval = EvalModel(rf_obj.model)
    rf_eval.get_test_pred()
    rf_eval.get_accuracy()
    rf_eval.get_f1()
    rf_eval.get_cm()
    rf_eval.get_confidence()

    #обучение RF
    rf_obj = RF(sdata, args.url)
    rf_obj.train()

    #обучение регрессионной модели 
    logreg_obj = LogReg(sdata, args.url)
    logreg_obj.train()

    #эвалюация регрессионной модели
    logreg_obj = EvalModel(rf_obj.model)
    logreg_obj.get_test_pred()
    logreg_obj.get_accuracy()
    logreg_obj.get_f1()
    logreg_obj.get_cm()
    logreg_obj.get_confidence()
