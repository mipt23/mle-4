import configparser
import json
from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext

class SparkData:
    #инициализация
    def __init__(self):
        self.spark_config_path = 'spark_config.json'
        self.spark_config = SparkConf()

        with open(self.spark_config_path, 'r') as config_json:
            spark_configs = json.load(config_json)

            for config in spark_configs:
                self.spark_config.set(config, spark_configs[config])

        self.sc = SparkContext.getOrCreate(conf=self.spark_config)

        self.db_config_path = '../config.ini'
        self.db_config = configparser.ConfigParser()
        self.db_config.read(self.config_path)

        self.gp = SQLContext(self.sc)
        self.gp_props = {   'user'      : self.db_config['DATABASE']['user'],
                            'password'  : self.db_config['DATABASE']['password'],
                            'driver'    : self.db_config['DATABASE']['driver']  }
    
    #чтение таблицы
    def read_db(self, table, url):
        self.df = self.gp.read.format(url=url, dbtable = table, 'io.pivotal.greenplum.spark.GreenplumRelationProvider').options(self.gp_props).load()
        return self.df 

    #разбиение на обучающую и тестовую выборки
    def split(self, train_size = 0.8, test_size = 0.2, train_table = "train", test_table = "test"):
        train, test = self.df.randomSplit([train_size, test_size])
        train.write.jdbc(url=self.url, table=train_table, mode="overwrite", properties=self.gp_props)
        test.write.jdbc(url=self.url, table=test_table, mode="overwrite", properties=self.gp_props)
    
    #запись таблицы
    def write_db(self, df, table, url):
        df.write.jdbc(url=url, table=table, mode="overwrite", properties=gp_props)
